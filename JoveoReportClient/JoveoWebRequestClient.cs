﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace JoveoReportClient
{
    class JoveoWebRequestClient
    {
        private readonly Uri statsUrl;
        private readonly string clientId;
        private readonly string apiKey;

        public JoveoWebRequestClient(string statsUrl, string clientId, string apiKey)
        {
            this.statsUrl = new Uri(statsUrl);
            this.clientId = clientId;
            this.apiKey = apiKey;
        }

        public string GetStats(JoveoRequestBody body)
        {
            var request = CreateRequest();
            AddBody(request, body);
            AddHeaders(request);
            return ReadResponseToEnd(request);
        }

        private HttpWebRequest CreateRequest()
        {
            var request = (HttpWebRequest)WebRequest.Create(statsUrl);
            request.Method = "POST";
            return request;
        }

        private void AddBody(HttpWebRequest request, JoveoRequestBody body)
        {
            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                streamWriter.Write(body.ToString());
            }
        }

        private void AddHeaders(WebRequest request)
        {
            request.ContentType = "application/json";
            request.Headers["x-api-key"] = apiKey;
            request.Headers["x-client-id"] = clientId;
        }

        private string ReadResponseToEnd(HttpWebRequest request)
        {
            //GetResponse can return exceptions - here should be Try/Catch block
            var response = (HttpWebResponse)request.GetResponse();
            return new StreamReader(response.GetResponseStream()).ReadToEnd();
        }
    }
}
