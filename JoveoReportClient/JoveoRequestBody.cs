﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoveoReportClient
{
    class JoveoRequestBody
    {
        //All available field should be prepared here
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public JoveoLevel FilterLevel { get; set; }
        public JoveoLevel GroupBy { get; set; }
        public bool GroupByPublisher { get; set; }

        private const string dateFormatter = "yyyy-MM-dd";

        public JoveoRequestBody()
        {
            //Set defaults which can be changed after object initialization
            StartDate = DateTime.Today.AddDays(-1);
            EndDate = DateTime.Today;
            FilterLevel = JoveoLevel.client;
            GroupBy = JoveoLevel.job;
            GroupByPublisher = true;
        }

        public override string ToString()
        {
            //The request body should be in json. As it's simple we can construct it without external library
            var builder = new StringBuilder();
            builder.Append("{\"filterBy\":{\"startDate\":\"");
            builder.Append(StartDate.ToString(dateFormatter));
            builder.Append("\",\"endDate\":\"");
            builder.Append(EndDate.ToString(dateFormatter));
            builder.Append("\",\"level\":\"");
            builder.Append(FilterLevel.ToString().ToLower());
            builder.Append("\"},\"groupBy\":\"");
            builder.Append(GroupBy.ToString().ToLower());
            builder.Append("\",\"groupByPublisher\":");
            builder.Append(GroupByPublisher.ToString().ToLower());
            builder.Append("}");
            return builder.ToString();
        }

    }

    enum JoveoLevel
    {
        job,
        jobgroup,
        campaign,
        client
    }
}
