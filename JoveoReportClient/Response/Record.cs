﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoveoReportClient.Response
{
    class Record
    {
        public Pivots Pivots { get; set; }
        public Stats Stats { get; set; }
        public Job Job { get; set; }
    }
}
