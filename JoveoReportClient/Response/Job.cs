﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoveoReportClient.Response
{
    class Job
    {
        public string JobId { get; set; }
        public string Title { get; set; }
        public string Category { get; set; }
        public string RefNumber { get; set; }
    }
}
