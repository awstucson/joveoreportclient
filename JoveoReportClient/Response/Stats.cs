﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoveoReportClient.Response
{
    class Stats
    {
        public int Applies { get; set; }
        public int ApplyStarts { get; set; }
        public int BotClicks { get; set; }
        public int Clicks { get; set; }
        public double Cpa { get; set; }
        public double Cpc { get; set; }
        public double Cta { get; set; }
        public int LatentClicks { get; set; }
        public double Spend { get; set; }
    }
}
