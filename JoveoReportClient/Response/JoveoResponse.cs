﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JoveoReportClient.Response
{
    class JoveoResponse
    {
        public bool Success { get; set; }
        public List<Record> Records { get; set; }
    }
}
