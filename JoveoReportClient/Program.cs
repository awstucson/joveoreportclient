﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JoveoReportClient.Response;
using System.Web.Script.Serialization;

namespace JoveoReportClient
{
    class Program
    {
        static void Main(string[] args)
        {
            //Data would come from configuration
            var statsUrl = "https://api.joveo.com/staging/stats";
            var clientId = "45186793-c57b-48c6-90ca-186ec543bfbc";
            var apiKey = "ArVO7nJsEK76oIrtTrhCj6FtIy5wgxUn6cTKwUIE";

            var joveoClient = new JoveoWebRequestClient(statsUrl, clientId, apiKey);

            //body comes with defaults
            var body = new JoveoRequestBody();

            //but you can customize it
            body.StartDate = new DateTime(2020, 1, 1);

            Console.WriteLine(body);

            var result = joveoClient.GetStats(body);

            //Deserialize to Joveo Respone
            var joveoResult = DeserializeResponsne(result);

            //Now you can check if the response is successful 
            if (joveoResult.Success)
            {
                //Take the data we need add the dates (from filter) around and insert into DB
                //You can use LINQ to query the data (using projections)
                var interesting_data = from r in joveoResult.Records
                    select new
                    {
                        jobId = r.Job.RefNumber,
                        clicks = r.Stats.Clicks,
                        applyStart = r.Stats.ApplyStarts,
                        distNetwork = r.Pivots.publisher
                    };

                //Insert into DB (printing to console here)
                foreach (var record in interesting_data)
                {
                    Console.WriteLine($"{record.jobId}\t{record.distNetwork}\t{record.clicks}");
                }

            }
            else
            {
                //Do whatever logging in case the response is not successful
                Console.WriteLine("Something went wrong");
            }

            //Result would have to be parsed and inserted into DB
            //It prints to console only
            //Console.WriteLine(result);
            Console.ReadLine();
        }

        static JoveoResponse DeserializeResponsne(string response)
        {
            var deserializer = new JavaScriptSerializer();
            return deserializer.Deserialize<JoveoResponse>(response);
        }
    }
}
